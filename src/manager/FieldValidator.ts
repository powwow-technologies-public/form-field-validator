import {ValidationType} from "../enum/ValidationType";
import {FieldValidationUtils} from "../util/FieldValidationUtils";
import {FieldValidationResponse} from "../model/FieldValidationResponse";
import {ValidationRules} from "../model/ValidationRules";
import {ValidationRule} from "../model/ValidationRule";
import {RegexPattern} from "../const/RegexPattern";
import IBAN = require('iban');

/**
 * Created by syntiro on 10/10/2017.
 */

export class FieldValidator{

    /**
     *
     * @param validationType
     * @param entry
     * @param confirmEntry
     */
    public getFieldValidationResponse(validationType: string, entry: string, confirmEntry?: string): any{

        let isValid = true;
        switch(validationType.toUpperCase()) {
            case ValidationType.toString(ValidationType.EMAIL):
                isValid = FieldValidationUtils.validateEmailAddress(entry);
                break;
            case ValidationType.toString(ValidationType.IBAN):
                isValid = IBAN.isValid(entry);
                break;
            case ValidationType.toString(ValidationType.BIC):
                isValid = FieldValidationUtils.validateBicNo(entry);
                break;
            case ValidationType.toString(ValidationType.PPS):
                isValid = FieldValidationUtils.validatePpsNo(entry);
                break;
            case ValidationType.toString(ValidationType.NUMBER):
                isValid = FieldValidationUtils.validateNumber(entry);
                break;
            case ValidationType.toString(ValidationType.ACCOUNT_NUMBER):
                isValid = FieldValidationUtils.validateAccountNumber(entry);
                break;
            case ValidationType.toString(ValidationType.NAME):
                isValid = FieldValidationUtils.validateName(entry);
                break;
            case ValidationType.toString(ValidationType.IRISH_LANDLINE_PHONE_NUMBER):
                isValid = FieldValidationUtils.validateIrishLandlinePhoneNumber(entry);
                break;
            case ValidationType.toString(ValidationType.IRISH_MOBILE_PHONE_NUMBER):
                isValid = FieldValidationUtils.validateIrishMobilePhoneNumber(entry);
                break;
            case ValidationType.toString(ValidationType.DATE):
                isValid = FieldValidationUtils.validateDate(entry);
                break;
            default:
                break;
        }
        if(isValid && confirmEntry){
            isValid = FieldValidationUtils.validateEntriesSame(entry, confirmEntry);
            validationType = ValidationType.toString(ValidationType.CONFIRM);
        }
        return new FieldValidationResponse(isValid, this.getFieldValidationMessage(validationType, isValid), validationType).toJSON();
    }

    private getFieldValidationMessage(validationType: string, isValid: boolean) {
        return isValid ? '' : ValidationType.getFieldValidationMessage(validationType);
    }

    /**
     *
     * @returns {Array<ValidationRule>}
     */
    public getValidationRules() : ValidationRules{
        let validationRules = new ValidationRules();
        for (let item in ValidationType) {
            if (isNaN(Number(item)) && item === item.toUpperCase() ) {
                let pattern = '';
                switch(item.toUpperCase()) {
                    case ValidationType.toString(ValidationType.EMAIL):
                        pattern = RegexPattern.EMAIL.toString();
                        break;
                    case ValidationType.toString(ValidationType.BIC):
                        pattern = RegexPattern.BIC.toString();
                        break;
                    case ValidationType.toString(ValidationType.PPS):
                        pattern = RegexPattern.PPS.toString();
                        break;
                    case ValidationType.toString(ValidationType.NUMBER):
                        pattern = RegexPattern.NUMBER.toString();
                        break;
                    case ValidationType.toString(ValidationType.ACCOUNT_NUMBER):
                        pattern = RegexPattern.ACCOUNT_NUMBER.toString();
                        break;
                    case ValidationType.toString(ValidationType.NAME):
                        pattern = RegexPattern.NAME.toString();
                        break;
                    case ValidationType.toString(ValidationType.IRISH_LANDLINE_PHONE_NUMBER):
                        pattern = RegexPattern.IRISH_LANDLINE_PHONE_NUMBER.toString();
                        break;
                    case ValidationType.toString(ValidationType.IRISH_MOBILE_PHONE_NUMBER):
                        pattern = RegexPattern.IRISH_MOBILE_PHONE_NUMBER.toString();
                        break;
                    case ValidationType.toString(ValidationType.DATE):
                        pattern = RegexPattern.DATE.toString();
                        break;
                    default:
                        break;
                }
                validationRules.addValidationRule(new ValidationRule(item, pattern))
            }
        }
        return validationRules;
    }

}
