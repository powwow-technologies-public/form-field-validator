import {RegexPattern} from "../const/RegexPattern";
/**
 * Created by syntiro on 11/10/2017.
 */
export class FieldValidationUtils{

    public static validateEmailAddress(entry: string): boolean{
        return RegexPattern.EMAIL.test(entry);
    }

    public static validateBicNo(entry: string): boolean{
        return RegexPattern.BIC.test(entry);
    }

    public static validatePpsNo(entry: string): boolean{
        return RegexPattern.PPS.test(entry);
    }

    public static validateNumber(entry: string): boolean{
        return RegexPattern.NUMBER.test(entry);
    }

    public static validateName(entry: string): boolean{
        return RegexPattern.NAME.test(entry);
    }

    public static validateAccountNumber(entry: string): boolean{
        return RegexPattern.ACCOUNT_NUMBER.test(entry);
    }

    public static validateIrishLandlinePhoneNumber(entry: string): boolean{
        return RegexPattern.IRISH_LANDLINE_PHONE_NUMBER.test(entry);
    }

    public static validateIrishMobilePhoneNumber(entry: string): boolean{
        return RegexPattern.IRISH_MOBILE_PHONE_NUMBER.test(entry);
    }

    public static validateDate(entry: string): boolean{
        return RegexPattern.DATE.test(entry);
    }

    public static validateEntriesSame(entry1: string, entry2: string): boolean{
        return entry1 === entry2;
    }

}
