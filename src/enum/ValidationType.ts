/**
 * Created by syntiro on 11/10/2017.
 */
export enum ValidationType{
    EMAIL = 1,
    IBAN = 2,
    PPS = 3,
    NUMBER = 4,
    NAME = 5,
    ACCOUNT_NUMBER = 6,
    IRISH_MOBILE_PHONE_NUMBER = 7,
    IRISH_LANDLINE_PHONE_NUMBER = 8,
    DATE = 9,
    CONFIRM = 10,
    BIC = 11,
}

export namespace ValidationType {

    export function isType(validationTypeStr: string, validationType: ValidationType){
        return ValidationType.toString(validationType) === validationTypeStr.toUpperCase();
    }

    export function toString(validationType: ValidationType): string {
        return ValidationType[validationType];
    }

    export function getFieldValidationMessage(validationType: string){
        switch (validationType.toUpperCase()){
            case ValidationType.toString(ValidationType.CONFIRM):
                return 'Values entered do not match';
            case ValidationType.toString(ValidationType.EMAIL):
            case ValidationType.toString(ValidationType.IBAN):
            case ValidationType.toString(ValidationType.PPS):
            case ValidationType.toString(ValidationType.NUMBER):
            case ValidationType.toString(ValidationType.NAME):
            case ValidationType.toString(ValidationType.ACCOUNT_NUMBER):
            case ValidationType.toString(ValidationType.IRISH_MOBILE_PHONE_NUMBER):
            case ValidationType.toString(ValidationType.IRISH_LANDLINE_PHONE_NUMBER):
            case ValidationType.toString(ValidationType.DATE):
            case ValidationType.toString(ValidationType.BIC):
                return 'This is not a valid '+validationType.toLowerCase();
            default:
                return '';
        }
    }
}