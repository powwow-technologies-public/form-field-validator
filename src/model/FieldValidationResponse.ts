/**
 * Created by syntiro on 11/10/2017.
 */
export class FieldValidationResponse{

    private valid: boolean;
    private message: string;
    private type: string;

    constructor(valid: boolean, message: string, type: string){
        this.valid = valid;
        this.message = message;
        this.type = type;
    }

    toJSON(){
        let obj = (<any>Object).assign({}, this, {
            // convert fields that need converting
        });
        return obj;
    }
}

