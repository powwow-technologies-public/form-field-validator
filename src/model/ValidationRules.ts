import {ValidationRule} from "./ValidationRule";
/**
 * Created by syntiro on 11/10/2017.
 */
export class ValidationRules{


    private validationRules: Array<ValidationRule>;

    constructor(){
        this.validationRules = [];
    }

    public addValidationRule(validationRule: ValidationRule){
        this.validationRules.push(validationRule);
    }

    public getValidationRules() : Array<ValidationRule>{
        return this.validationRules;
    }

    public contains(validationRule: ValidationRule){
        let i, len;
        for (i = 0, len = this.validationRules.length; i < len; i++) {
            if(this.validationRules[i].getType() == validationRule.getType() &&
                this.validationRules[i].getExpression().toString() === validationRule.getExpression().toString()){
                return true;
            }
        }
        return false;
    }

    toJSON(){
        let i, len, json: Array<ValidationRule> = [];
        for (i = 0, len = this.validationRules.length; i < len; i++) {
            json.push(this.validationRules[i].toJSON());
        }
        return json;
    }

}

