/**
 * Created by syntiro on 11/10/2017.
 */
export class ValidationRule{

    private type: string;
    private expression: string;

    constructor(type: string, expression: string){
        this.type = type;
        this.expression = expression;
    }

    public getType(){
        return this.type;
    }

    public getExpression(){
        return this.expression;
    }

    toJSON(){
        let obj = (<any>Object).assign({}, this, {
            // convert fields that need converting
        });
        return obj;
    }

}