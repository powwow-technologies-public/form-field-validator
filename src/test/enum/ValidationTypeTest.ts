import {ValidationType} from "../../enum/ValidationType";
import {TestCase, Expect} from "alsatian";
/**
 * Created by syntiro on 14/11/2017.
 */

export class ValidationTypeTest{

    @TestCase(ValidationType.toString(ValidationType.EMAIL))
    @TestCase(ValidationType.toString(ValidationType.ACCOUNT_NUMBER))
    @TestCase(ValidationType.toString(ValidationType.IRISH_MOBILE_PHONE_NUMBER))
    @TestCase(ValidationType.toString(ValidationType.IRISH_LANDLINE_PHONE_NUMBER))
    @TestCase(ValidationType.toString(ValidationType.NAME))
    @TestCase(ValidationType.toString(ValidationType.NUMBER))
    @TestCase(ValidationType.toString(ValidationType.PPS))
    @TestCase(ValidationType.toString(ValidationType.IBAN))
    @TestCase(ValidationType.toString(ValidationType.DATE))
    @TestCase(ValidationType.toString(ValidationType.BIC))
    @TestCase(ValidationType.toString(ValidationType.CONFIRM))
    @TestCase('')
    public testGetFieldValidationMessage(validationType: string){
        let compareMessage = '';
        switch(validationType) {
            case ValidationType.toString(ValidationType.CONFIRM):
                compareMessage = 'Values entered do not match';
                break;
            case '':
                compareMessage = '';
                break;
            default:
                compareMessage = 'This is not a valid '+validationType.toLowerCase();
                break;
        }
        let message = ValidationType.getFieldValidationMessage(validationType);
        Expect(message).toEqual(compareMessage);
    }

    @TestCase(ValidationType.toString(ValidationType.EMAIL), ValidationType.EMAIL)
    @TestCase(ValidationType.toString(ValidationType.ACCOUNT_NUMBER), ValidationType.ACCOUNT_NUMBER)
    @TestCase(ValidationType.toString(ValidationType.IRISH_MOBILE_PHONE_NUMBER), ValidationType.IRISH_MOBILE_PHONE_NUMBER)
    @TestCase(ValidationType.toString(ValidationType.IRISH_LANDLINE_PHONE_NUMBER), ValidationType.IRISH_LANDLINE_PHONE_NUMBER)
    @TestCase(ValidationType.toString(ValidationType.NAME), ValidationType.NAME)
    @TestCase(ValidationType.toString(ValidationType.NUMBER), ValidationType.NUMBER)
    @TestCase(ValidationType.toString(ValidationType.PPS), ValidationType.PPS)
    @TestCase(ValidationType.toString(ValidationType.IBAN), ValidationType.IBAN)
    @TestCase(ValidationType.toString(ValidationType.DATE), ValidationType.DATE)
    @TestCase(ValidationType.toString(ValidationType.BIC), ValidationType.BIC)
    @TestCase(ValidationType.toString(ValidationType.CONFIRM), ValidationType.CONFIRM)
    public testIsType(validationTypeStr: string, validationType: ValidationType){
        let isType = ValidationType.isType(validationTypeStr, validationType);
        Expect(isType).toEqual(true);
    }

}