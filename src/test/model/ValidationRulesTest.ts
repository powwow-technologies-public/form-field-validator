/**
 * Created by syntiro on 04/12/2017.
 */
import {ValidationRule} from "../../model/ValidationRule";
import {ValidationType} from "../../enum/ValidationType";
import {RegexPattern} from "../../const/RegexPattern";
import {TestCase, Expect} from "alsatian";
import {ValidationRules} from "../../model/ValidationRules";

export class ValidationRulesTest{

    @TestCase(new ValidationRule(ValidationType.toString(ValidationType.EMAIL), RegexPattern.EMAIL.toString()))
    public testAddValidationRule(validationRule: ValidationRule){
        let validationRules = new ValidationRules();
        validationRules.addValidationRule(validationRule);
        Expect(validationRules.getValidationRules().length).toEqual(1);
    }

    @TestCase(new ValidationRule(ValidationType.toString(ValidationType.EMAIL), RegexPattern.EMAIL.toString()))
    public testContains(containsValidationRule: ValidationRule){
        let validationRules = new ValidationRules();
        validationRules.addValidationRule(containsValidationRule);
        Expect(validationRules.contains(containsValidationRule)).toEqual(true);
    }

    @TestCase(new ValidationRule(ValidationType.toString(ValidationType.EMAIL), RegexPattern.EMAIL.toString()), new ValidationRule(ValidationType.toString(ValidationType.ACCOUNT_NUMBER), RegexPattern.ACCOUNT_NUMBER.toString()))
    public testNotContains(containsValidationRule: ValidationRule, notContainsValidationRule: ValidationRule){
        let validationRules = new ValidationRules();
        validationRules.addValidationRule(containsValidationRule);
        Expect(validationRules.contains(notContainsValidationRule)).toEqual(false);
    }

    @TestCase(new ValidationRule(ValidationType.toString(ValidationType.EMAIL), RegexPattern.EMAIL.toString()), [{"type":"EMAIL","expression":"/^(([^<>()[\\]\\\\.,;:\\s@\\\"]+(\\.[^<>()[\\]\\\\.,;:\\s@\\\"]+)*)|(\\\".+\\\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$/"}])
    public testToJSON(containsValidationRule: ValidationRule, expected: any){
        let validationRules = new ValidationRules();
        validationRules.addValidationRule(containsValidationRule);
        Expect(validationRules.toJSON()).toEqual(expected);
    }

}
