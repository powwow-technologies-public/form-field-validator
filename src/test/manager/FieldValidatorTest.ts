import {Expect, SpyOn, Test, TestCase} from "alsatian";
import {ValidationType} from "../../enum/ValidationType";
import {FieldValidator} from "../../manager/FieldValidator";
import {FieldValidationResponse} from "../../model/FieldValidationResponse";
import {ValidationRules} from "../../model/ValidationRules";
import {ValidationRule} from "../../model/ValidationRule";
import {RegexPattern} from "../../const/RegexPattern";
/**
 * Created by syntiro on 14/11/2017.
 */

export class FieldValidatorTest{

    @TestCase(ValidationType.toString(ValidationType.EMAIL), '123456', false)
    @TestCase(ValidationType.toString(ValidationType.EMAIL), 'good@email.test', true)
    @TestCase(ValidationType.toString(ValidationType.ACCOUNT_NUMBER), 'not-this', false)
    @TestCase(ValidationType.toString(ValidationType.ACCOUNT_NUMBER), '12345678', true)
    @TestCase(ValidationType.toString(ValidationType.IRISH_MOBILE_PHONE_NUMBER), '018080028', false)
    @TestCase(ValidationType.toString(ValidationType.IRISH_MOBILE_PHONE_NUMBER), '0868080028', true)
    @TestCase(ValidationType.toString(ValidationType.IRISH_LANDLINE_PHONE_NUMBER), '01-8080028', false)
    @TestCase(ValidationType.toString(ValidationType.IRISH_LANDLINE_PHONE_NUMBER), '018080028', true)
    @TestCase(ValidationType.toString(ValidationType.NAME), 'R2D2', false)
    @TestCase(ValidationType.toString(ValidationType.NAME), 'Batman', true)
    @TestCase(ValidationType.toString(ValidationType.NUMBER), '12E456', false)
    @TestCase(ValidationType.toString(ValidationType.NUMBER), '123456', true)
    @TestCase(ValidationType.toString(ValidationType.PPS), 'G1234567', false)
    @TestCase(ValidationType.toString(ValidationType.PPS), '1234567G', true)
    @TestCase(ValidationType.toString(ValidationType.IBAN), 'IE64BOFI90583812345678', false)
    @TestCase(ValidationType.toString(ValidationType.IBAN), 'IE 94IPBS 990643 24173574', true)
    @TestCase(ValidationType.toString(ValidationType.DATE), '12-12-2017', false)
    @TestCase(ValidationType.toString(ValidationType.DATE), '12/12/2017', true)
    @TestCase(ValidationType.toString(ValidationType.BIC), 'BOFIIER2D2', false)
    @TestCase(ValidationType.toString(ValidationType.BIC), 'BOFIIE2D', true)
    @TestCase(ValidationType.toString(ValidationType.CONFIRM), 'good@email.test', false, '123456')
    @TestCase(ValidationType.toString(ValidationType.CONFIRM), 'good@email.test', true, 'good@email.test')
    public testGetFieldValidationResponse(validationType: string, entry: string, responseFlag: boolean, confirmEntry?: string){
        let compareFieldValidationResponse: FieldValidationResponse = new FieldValidationResponse(responseFlag, 'mock validation message', validationType);
        let fieldValidator: FieldValidator = new FieldValidator();
        SpyOn(fieldValidator, "getFieldValidationMessage").andReturn('mock validation message');
        let fieldValidationResponseJSON: any = fieldValidator.getFieldValidationResponse(validationType, entry, confirmEntry);
        Expect(fieldValidationResponseJSON).toEqual(compareFieldValidationResponse.toJSON());
    }

    @TestCase(ValidationType.toString(ValidationType.EMAIL), RegexPattern.EMAIL)
    @TestCase(ValidationType.toString(ValidationType.ACCOUNT_NUMBER), RegexPattern.ACCOUNT_NUMBER)
    @TestCase(ValidationType.toString(ValidationType.IRISH_MOBILE_PHONE_NUMBER), RegexPattern.IRISH_MOBILE_PHONE_NUMBER)
    @TestCase(ValidationType.toString(ValidationType.IRISH_LANDLINE_PHONE_NUMBER), RegexPattern.IRISH_LANDLINE_PHONE_NUMBER)
    @TestCase(ValidationType.toString(ValidationType.NAME), RegexPattern.NAME)
    @TestCase(ValidationType.toString(ValidationType.NUMBER), RegexPattern.NUMBER)
    @TestCase(ValidationType.toString(ValidationType.PPS), RegexPattern.PPS)
    @TestCase(ValidationType.toString(ValidationType.DATE), RegexPattern.DATE)
    @TestCase(ValidationType.toString(ValidationType.BIC), RegexPattern.BIC)
    public testGetValidationRules(validationType: string, regexPattern: string) {
        let compareValidationRule = new ValidationRule(validationType,regexPattern);
        let fieldValidator: FieldValidator = new FieldValidator();
        let fieldValidationRules: ValidationRules = fieldValidator.getValidationRules();
        Expect(fieldValidationRules.contains(compareValidationRule)).toEqual(true);
    }

}