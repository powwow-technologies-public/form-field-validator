/**
 * Created by syntiro on 11/10/2017.
 */
export class RegexPattern{

    public static readonly EMAIL = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    public static readonly PPS = /\d{7}[a-zA-Z]{1,2}$/;

    public static readonly BIC = /^([a-zA-Z]{4}[a-zA-Z]{2}[a-zA-Z0-9]{2}([a-zA-Z0-9]{3})?)$/i;

    public static readonly NUMBER = /^-?[0-9]\d*(\.\d+)?$/;

    public static readonly NAME = /^[a-zA-Z\-'\sdÀÈÌÒÙàèìòùÁÉÍÓÚÝáéíóúýÂÊÎÔÛâêîôûÃÑÕãñõÄËÏÖÜäëïöüçÇßØøÅåÆæÞþÐð\.]*$/;

    public static readonly ACCOUNT_NUMBER = /^\d{8}(?:\d{0})?$/;

    public static readonly IRISH_LANDLINE_PHONE_NUMBER = /^\s*(\(?\s*\d{1,4}\s*\)?\s*[\d\s]{5,10})\s*$/;

    public static readonly IRISH_MOBILE_PHONE_NUMBER = /^08\d{8}$/;

    public static readonly DATE = /^(\d{2})\/(\d{2})\/(\d{4})$/;

}