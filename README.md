#form-field-validator

Node module written in typescript for common form field validation rules returning FieldValidationResponse valid status and validation message

The following field types are supported
- EMAIL = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
- PPS = /\d{7}[a-zA-Z]{1,2}$/;
- BIC = /^([a-zA-Z]{4}[a-zA-Z]{2}[a-zA-Z0-9]{2}([a-zA-Z0-9]{3})?)$/i;
- NUMBER = /^-?[0-9]\d*(\.\d+)?$/;
- NAME = /^[a-zA-Z\-'\sdÀÈÌÒÙàèìòùÁÉÍÓÚÝáéíóúýÂÊÎÔÛâêîôûÃÑÕãñõÄËÏÖÜäëïöüçÇßØøÅåÆæÞþÐð\.]*$/;
- ACCOUNT_NUMBER = /^\d{8}(?:\d{0})?$/;
- IRISH_LANDLINE_PHONE_NUMBER = /^\s*(\(?\s*\d{1,4}\s*\)?\s*[\d\s]{5,10})\s*$/;
- IRISH_MOBILE_PHONE_NUMBER = /^08\d{8}$/;
- DATE = /^(\d{2})\/(\d{2})\/(\d{4})$/;
- IBAN = IBAN.js - https://www.npmjs.com/package/iban - follows the ISO 13616 IBAN Registry technical specification.

## Installation

A simple form field validator library using Typescript

```
npm install form-field-validator

```

This is a client or server side script 

If using it client side you'll need to include the fields-validation-bundle.js file

Re-compiling fields-validation-bundle.js can be done by calling

```
npm run browserify

```

## Usage

Basic IBAN validation check 
```js
formFieldValidator.newInstance().getFieldValidationResponse('IBAN', 'IE 94IPBS 990643 24173574');
```
This will return the following validation response

```js
{
    valid: true, 
    message: '', 
    type: 'IBAN'
}
```

Confirm validation check with emails coming from 2 separate fields 

```js

formFieldValidator.newInstance().getFieldValidationResponse('CONFIRM', 'valid@email.check', 'valid@email.check');

```
This will return the following validation response

```js
{
    valid: true, 
    message: '', 
    type: 'CONFIRM'
}
```

An invalid validation response will look something like this

```js
{
    valid: false, 
    message: 'This is not a valid iban', 
    type: 'IBAN'
}
```

## Unit Testing
```
npm run build

npm run test

```
### Unit Test Code Coverage Report

```
npm run check-coverage

```

